package com.example.graph;

import com.google.inject.Inject;

class D {

	private E e;

	@Inject
	public D(E e) {
		super();
		this.e = e;
		System.out.println("D costructor called");
	}
}
