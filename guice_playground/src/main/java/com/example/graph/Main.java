package com.example.graph;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;

public class Main {

	public Main() {
		Injector injector = Guice.createInjector(new GuiceModule());

		for (Key<?> key : injector.getAllBindings().keySet()) {
			System.out.println(key);
		}
		
		System.out.println("--------------------------------------------");
		
		A a = injector.getInstance(A.class);

		for (Key<?> key : injector.getAllBindings().keySet()) {
			System.out.println(key);
		}
		
		System.out.println("--------------------------------------------");
		
		a.foo();
		
		

	}

	/**
	 * Main method.
	 */
	public static void main(String[] args) {
		new Main();
	}
}
