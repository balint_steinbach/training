package com.example.graph;

import com.google.inject.AbstractModule;

class GuiceModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(MyInterface.class).to(MyInterfaceImpl.class);
	}

}
