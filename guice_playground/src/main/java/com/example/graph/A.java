package com.example.graph;

import com.google.inject.Inject;

class A {

	private B b;

	private C c;

	private D d;

	private MyInterface myInterface;

	@Inject
	public A(B b, C c, D d, MyInterface myInterface) {
		this.b = b;
		this.c = c;
		this.d = d;
		this.myInterface = myInterface;
		System.out.println("A costructor called");
	}

	public void foo() {
		myInterface.foo();
	}
}
