package com.example.injectiontypes;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

class GuiceModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(String.class).annotatedWith(Names.named("parameter")).toInstance("Parameter");
	}

}
