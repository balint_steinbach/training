package com.example.injectiontypes;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class Main {

	public Main() {

		Injector injector = Guice.createInjector(new GuiceModule());

		A a = injector.getInstance(A.class);

		System.out.println("B: " + a.getB()+ " C: " + a.getC() + " D: " + a.getD() + " parameter: " + a.getParameter());

	}

	/**
	 * Main method.
	 */
	public static void main(String[] args) {
		new Main();
	}


}
