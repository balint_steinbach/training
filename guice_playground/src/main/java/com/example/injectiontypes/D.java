package com.example.injectiontypes;

import com.google.inject.Inject;

class D {

	private E e;

	@Inject
	public D(E e) {
		super();
		this.e = e;
	}
}
