package com.example.injectiontypes;

import com.google.inject.Inject;
import com.google.inject.name.Named;


class A {

	private B b;

	@Inject
	private C c;

	private D d;

	private String parameter;

	@Inject
	public A(@Named("parameter") String parameter, B b) {
		this.b = b;
		this.parameter = parameter;
	};

	@Inject
	public void setD(D d) {
		this.d = d;
	}

	public B getB() {
		return b;
	}

	public void setB(B b) {
		this.b = b;
	}

	public C getC() {
		return c;
	}

	public void setC(C c) {
		this.c = c;
	}

	public D getD() {
		return d;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
}
