package com.example.circulardependencies;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class Main {

	/**
	 * Main method.
	 */
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		System.out.println("Initializing Guice context.");

		Injector injector = Guice.createInjector(new GuiceModule());

		A a = injector.getInstance(A.class);

		System.out.println("Guice context initialized in: " + (System.currentTimeMillis() - start) + " ms");

	}

}
