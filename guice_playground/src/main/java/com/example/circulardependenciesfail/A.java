package com.example.circulardependenciesfail;

import com.google.inject.Inject;

public class A {

	private B b;

	@Inject
	public A(B b) {
		super();
		this.b = b;
	}

}
