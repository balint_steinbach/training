package com.example.circulardependenciesfail;

import com.google.inject.Inject;

public class B {

	private A a;

	@Inject
	public B(A a) {
		super();
		this.a = a;
	}

}
