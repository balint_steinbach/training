package com.example.factory;

import java.util.concurrent.ExecutorService;

import com.google.inject.Inject;

public class BuyService {
	
	private PaymentFactory paymentFactory;
	private ExecutorService executorService;

	@Inject
	public BuyService(PaymentFactory paymentFactory, ExecutorService executorService) {
		this.paymentFactory = paymentFactory;
		this.executorService = executorService;
		
	}
	
	public void pay(boolean isPaypal, final String amountInHuf){
		final Payment payment;
		if(isPaypal) {
			payment = paymentFactory.getPayPalPayment(true);
		} else {
			payment = paymentFactory.getCreditCardPayment(true);
		}
		executorService.submit(new Runnable() {
			
			public void run() {
				payment.pay(amountInHuf);
			}
		});
		
	}

}
