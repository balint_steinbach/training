package com.example.factory;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MoneyExchengeService {
	
	public String getHufAmountInUsd(String hufAmount){
		BigDecimal amount = new BigDecimal(hufAmount);
		return amount.divide(new BigDecimal("288.456"), 3, RoundingMode.HALF_UP).toString();
	}

}
