package com.example.factory;

public abstract class Payment {
	
	private MoneyExchengeService moneyExchengeService;
	protected Boolean isTest;
	
	public Payment(MoneyExchengeService moneyExchengeService, Boolean isTest) {
		super();
		this.moneyExchengeService = moneyExchengeService;
		this.isTest = isTest;
	}

	public void pay(String amountInHuf){
		String amountInUsd = moneyExchengeService.getHufAmountInUsd(amountInHuf);
		payInUsd(amountInUsd);
	}
	
	protected abstract void payInUsd(String amountInUsd);
}
