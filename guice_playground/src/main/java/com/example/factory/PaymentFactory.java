package com.example.factory;

import com.google.inject.assistedinject.Assisted;
import com.google.inject.name.Named;

public interface PaymentFactory {
	
	@Named("PayPal") public PayPalPayment getPayPalPayment(@Assisted Boolean isTest);
	@Named("CreditCard") public CreditCardPayment getCreditCardPayment(@Assisted Boolean isTest);
}
