package com.example.factory;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class Main {

	/**
	 * Main method.
	 */
	public static void main(String[] args) {
		
		boolean isPayPal = true;

		Injector injector = Guice.createInjector(new GuiceModule());

		BuyService buyService = injector.getInstance(BuyService.class);
		buyService.pay(isPayPal, "1000");

	}

}
