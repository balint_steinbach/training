package com.example.factory;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

public class CreditCardPayment extends Payment{

	@Inject
	public CreditCardPayment(@Assisted Boolean isTest, MoneyExchengeService moneyExchengeService) {
		super(moneyExchengeService, isTest);
	}

	@Override
	protected void payInUsd(String amountInUsd) {
		System.out.println("Credit card payment: "+ amountInUsd + " USD");
		System.out.println("Is test payment: "+ isTest);
	}

	

}
