package com.example.factory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.google.inject.AbstractModule;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.name.Names;

class GuiceModule extends AbstractModule {

	@Override
	protected void configure() {
		install(new FactoryModuleBuilder()
			     .implement(Payment.class, Names.named("PayPal"), PayPalPayment.class)
			     .implement(Payment.class, Names.named("CreditCard"), CreditCardPayment.class)
			     .build(PaymentFactory.class));
		
		bind(ExecutorService.class).toInstance(Executors.newFixedThreadPool(2));
		
	}

}
