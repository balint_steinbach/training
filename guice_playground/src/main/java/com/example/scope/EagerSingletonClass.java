package com.example.scope;

public class EagerSingletonClass {
	
	public EagerSingletonClass() {
		System.out.println("EagerSingleton initalized");
	}
}
