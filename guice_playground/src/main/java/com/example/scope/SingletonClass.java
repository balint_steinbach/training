package com.example.scope;

import javax.inject.Singleton;

@Singleton
public class SingletonClass {
	
	public SingletonClass() {
		System.out.println("Singleton class created");
	}

	private int counter = 0;

	public void incrementCounter() {
		counter++;
	}

	public void print() {
		System.out.println("Singleton counter: " + counter);
	}

}
