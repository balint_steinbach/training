package com.example.scope;

public class PrototypeClass {
	
	
	private int counter = 0;
	
	public PrototypeClass() {
		System.out.println("Prototype class created");
	}

	public void incrementCounter() {
		counter++;
	}

	public void print() {
		System.out.println("Prototype counter: " + counter);
	}
}
