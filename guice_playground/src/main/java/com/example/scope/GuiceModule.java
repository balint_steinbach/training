package com.example.scope;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

class GuiceModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(SingletonClass.class).in(Singleton.class);
		bind(EagerSingletonClass.class).asEagerSingleton();
		
	}

}
