package com.example.scope;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class Main {

	/**
	 * Main method.
	 */
	public static void main(String[] args) {
		
		Injector injector = Guice.createInjector(new GuiceModule());

		System.out.println("Get instance from class A");
		A a = injector.getInstance(A.class);

		a.increment();
		a.print();

		B b = injector.getInstance(B.class);

		b.increment();
		b.print();
	}

}
