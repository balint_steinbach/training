package com.example.aop;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class NewRequestMethodIntercepor implements MethodInterceptor {

	public Object invoke(MethodInvocation invocation) throws Throwable {
		Object argument1 = invocation.getArguments()[1];
		if ((Integer) argument1 == 0) {
			throw new IllegalArgumentException("Don't event think about giving 0 point for a story.");
		}
		return invocation.proceed();
	}

}
