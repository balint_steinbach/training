package com.example.aop;

public class ProjectOwnerRequestProcessor {

	@DontEvenThinkAboutIt
	public void addNewProjectOwnerRequest(String request, int estimate) {
		System.out.println("Po req added: " + request + " estimate: " + estimate);
	}
}
