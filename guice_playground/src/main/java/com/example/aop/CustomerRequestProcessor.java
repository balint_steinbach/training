package com.example.aop;

public class CustomerRequestProcessor {

	@DontEvenThinkAboutIt
	public void addNewCustomerRequest(String request, int estimate) {
		System.out.println("Customer req added: " + request + " estimate: " + estimate);
	}
}
