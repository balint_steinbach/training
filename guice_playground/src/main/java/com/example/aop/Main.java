package com.example.aop;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class Main {

	/**
	 * Main method.
	 */
	public static void main(String[] args) {
		Injector injector = Guice.createInjector(new GuiceModule());
		CustomerRequestProcessor customerRequestProcessor = injector.getInstance(CustomerRequestProcessor.class);
		ProjectOwnerRequestProcessor projectOwnerRequestProcessor = injector
				.getInstance(ProjectOwnerRequestProcessor.class);

		customerRequestProcessor.addNewCustomerRequest("Redisign login", 1);
		projectOwnerRequestProcessor.addNewProjectOwnerRequest("Redisign login", 1);
		try {
			customerRequestProcessor.addNewCustomerRequest("Replace all logo", 0);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		try {
			projectOwnerRequestProcessor.addNewProjectOwnerRequest("Replace all logo", 0);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
	}

}
