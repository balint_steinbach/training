package com.example.aop;

import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;

class GuiceModule extends AbstractModule {

	@Override
	protected void configure() {
		bindInterceptor(Matchers.any(), Matchers.annotatedWith(DontEvenThinkAboutIt.class),
				new NewRequestMethodIntercepor());
	}

}
