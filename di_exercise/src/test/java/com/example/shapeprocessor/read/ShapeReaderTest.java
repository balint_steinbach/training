package com.example.shapeprocessor.read;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.example.shapeprocessor.Globals;
import com.example.shapeprocessor.read.ShapeParser;
import com.example.shapeprocessor.read.ShapeReader;
import com.example.shapeprocessor.read.ShapeReaderImpl;
import com.example.shapeprocessor.service.ShapeService;
import com.example.shapeprocessor.shapes.Circle;
import com.example.shapeprocessor.shapes.OneParameterShape;
import com.example.shapeprocessor.shapes.Squere;

@RunWith(MockitoJUnitRunner.class)
public class ShapeReaderTest {

	private ShapeReader target;

	@Mock
	private ShapeService shapeService;

	@Mock
	private ShapeParser shapeParser;

	@Before
	public void setup() {
		target = new ShapeReaderImpl(shapeService, shapeParser);
	}

	@Test
	public void readCircleTest() {

		Mockito.when(shapeParser.parseShapeMessage(Mockito.anyString())).thenReturn(new Circle(10));
		OneParameterShape shape = target.getShape(Globals.CIRCLE);

		Assert.assertNotNull(shape);
		Assert.assertTrue(shape instanceof Circle);
		Assert.assertEquals(10, shape.getParameter());
		Assert.assertEquals(Globals.CIRCLE, shape.getName());

		Mockito.verify(shapeService).getCircle();
	}

	@Test
	public void readSquareTest() {

		Mockito.when(shapeParser.parseShapeMessage(Mockito.anyString())).thenReturn(new Squere(10));
		OneParameterShape shape = target.getShape(Globals.SQUARE);

		Assert.assertNotNull(shape);
		Assert.assertTrue(shape instanceof Squere);
		Assert.assertEquals(10, shape.getParameter());
		Assert.assertEquals(Globals.SQUARE, shape.getName());

		Mockito.verify(shapeService).getSquare();
	}

	@Test(expected = IllegalArgumentException.class)
	public void readunknownShapeName() {
		target.getShape("not existing shape");
	}

}
