package com.example.shapeprocessor.read;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.example.shapeprocessor.Globals;
import com.example.shapeprocessor.shapes.Circle;
import com.example.shapeprocessor.shapes.OneParameterShape;
import com.example.shapeprocessor.shapes.Squere;

@RunWith(MockitoJUnitRunner.class)
public class ShapeParserTest {

	private ShapeParser target;

	@Before
	public void setup() {
		target = new ShapeParserImpl();
	}

	@Test
	public void parseCircle() {
		OneParameterShape shape = target.parseShapeMessage(Globals.CIRCLE + " 10");
		Assert.assertNotNull(shape);
		Assert.assertTrue(shape instanceof Circle);
		Assert.assertEquals(Globals.CIRCLE, shape.getName());
		Assert.assertEquals(10, shape.getParameter());
	}

	@Test
	public void parseSqueare() {
		OneParameterShape shape = target.parseShapeMessage(Globals.SQUARE + " 10");
		Assert.assertNotNull(shape);
		Assert.assertTrue(shape instanceof Squere);
		Assert.assertEquals(Globals.SQUARE, shape.getName());
		Assert.assertEquals(10, shape.getParameter());
	}

	@Test(expected = IllegalArgumentException.class)
	public void parseFailure() {
		target.parseShapeMessage("Non existing shape" + " 10");
	}

}
