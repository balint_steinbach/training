package com.example.shapeprocessor;

import java.util.concurrent.ExecutorService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.example.shapeprocessor.calc.CalculationTask;
import com.example.shapeprocessor.calc.CalculationTaskFactory;
import com.example.shapeprocessor.calc.StatCalculationCallback;
import com.example.shapeprocessor.calc.StatCalculationResult;
import com.example.shapeprocessor.calc.StatCalculator;
import com.example.shapeprocessor.calc.StatCalculatorImpl;
import com.example.shapeprocessor.shapes.Circle;

@RunWith(MockitoJUnitRunner.class)
public class StatCalculatorTest {

	private StatCalculator target;
	@Mock
	private ExecutorService executorService;
	@Mock
	private CalculationTaskFactory calculationTaskFactory;

	private StatCalculationCallback cbk = new StatCalculationCallback() {

		@Override
		public void onCalculationFinished(StatCalculationResult statCalculationResult) {
			// TODO Auto-generated method stub

		}
	};

	@Before
	public void setup() {
		target = new StatCalculatorImpl(executorService, calculationTaskFactory);
	}

	@Test
	public void calculateCirle() {
		Circle shape = new Circle(10);
		target.submitCalculation(shape, cbk);

		Mockito.verify(calculationTaskFactory).createTask(shape, cbk);
		Mockito.verify(executorService).submit(Mockito.any(CalculationTask.class));
	}

}
