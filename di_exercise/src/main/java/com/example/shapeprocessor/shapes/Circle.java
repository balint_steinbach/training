package com.example.shapeprocessor.shapes;

import javax.inject.Inject;

import com.example.shapeprocessor.Globals;

public class Circle extends OneParameterShape {

	@Inject
	public Circle(int parameter) {
		super(parameter, Globals.CIRCLE);
	}

	@Override
	protected String getParameterName() {
		return "radius";
	}
}
