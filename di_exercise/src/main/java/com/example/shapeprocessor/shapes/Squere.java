package com.example.shapeprocessor.shapes;

import com.example.shapeprocessor.Globals;

public class Squere extends OneParameterShape {

	public Squere(int parameter) {
		super(parameter, Globals.SQUARE);
	}

	@Override
	protected String getParameterName() {
		return "side";
	}

}
