package com.example.shapeprocessor.shapes;

public abstract class OneParameterShape {

	protected int parameter;

	protected String name;

	public OneParameterShape(int parameter, String name) {
		this.parameter = parameter;
		this.name = name;
	}

	public int getParameter() {
		return parameter;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name + " " + getParameterName() + ": " + parameter;
	}

	protected abstract String getParameterName();
}
