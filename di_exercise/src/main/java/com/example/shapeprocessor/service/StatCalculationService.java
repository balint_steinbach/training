package com.example.shapeprocessor.service;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.example.shapeprocessor.Globals;
import com.example.shapeprocessor.calc.StatCalculationResult;
import com.example.shapeprocessor.shapes.OneParameterShape;

@Singleton
public class StatCalculationService {

	@Inject
	public StatCalculationService() {
		// TODO Auto-generated constructor stub
	}

	public StatCalculationResult calculate(OneParameterShape shape) {
		StatCalculationResult result = new StatCalculationResult();
		double area;
		if (Globals.CIRCLE.equals(shape.getName())) {
			area = shape.getParameter() * shape.getParameter() * Math.PI;
		} else {
			area = shape.getParameter() * shape.getParameter();
		}
		result.setArea(area);
		return result;
	}
}
