package com.example.shapeprocessor.service;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.example.shapeprocessor.Globals;

@Singleton
public class ShapeService {

	@Inject
	public ShapeService() {
		// TODO Auto-generated constructor stub
	}

	public String getCircle() {
		return Globals.CIRCLE + " " + (int) (Math.random() * 10);
	}

	public String getSquare() {
		return Globals.SQUARE + " " + (int) (Math.random() * 10);
	}

}
