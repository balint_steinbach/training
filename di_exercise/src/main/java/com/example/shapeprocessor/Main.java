package com.example.shapeprocessor;

public class Main {

	public static void main(String[] args) {
		App app = DaggerAppComponent.create().createApp();

		app.userClickedOnCircle();
		app.userClickedOnSquare();
	}

}
