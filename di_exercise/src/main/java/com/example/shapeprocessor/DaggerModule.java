package com.example.shapeprocessor;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.inject.Singleton;

import com.example.shapeprocessor.calc.StatCalculator;
import com.example.shapeprocessor.calc.StatCalculatorImpl;
import com.example.shapeprocessor.read.ShapeParser;
import com.example.shapeprocessor.read.ShapeParserImpl;
import com.example.shapeprocessor.read.ShapeReader;
import com.example.shapeprocessor.read.ShapeReaderImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class DaggerModule {

	@Provides
	@Singleton
	public ShapeReader provideReader(ShapeReaderImpl shapeReaderImpl) {
		return shapeReaderImpl;
	}

	@Provides
	@Singleton
	public ShapeParser provideParser(ShapeParserImpl shapeParserImpl) {
		return shapeParserImpl;
	}

	@Provides
	@Singleton
	public StatCalculator provideStatCalculator(StatCalculatorImpl statCalculatorImpl) {
		return statCalculatorImpl;
	}

	@Provides
	@Singleton
	public ExecutorService executorService() {
		return Executors.newFixedThreadPool(2);
	}

}
