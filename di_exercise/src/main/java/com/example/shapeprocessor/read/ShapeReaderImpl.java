package com.example.shapeprocessor.read;

import javax.inject.Inject;

import com.example.shapeprocessor.Globals;
import com.example.shapeprocessor.service.ShapeService;
import com.example.shapeprocessor.shapes.OneParameterShape;

public class ShapeReaderImpl implements ShapeReader {

	private ShapeService shapeService;
	private ShapeParser shapeParser;

	@Inject
	public ShapeReaderImpl(ShapeService shapeService, ShapeParser shapeParser) {
		this.shapeService = shapeService;
		this.shapeParser = shapeParser;
	}

	@Override
	public OneParameterShape getShape(String shapeName) {
		String message = callShapeService(shapeName);
		return shapeParser.parseShapeMessage(message);
	}

	private String callShapeService(String shapeName) {
		if (shapeName.equals(Globals.CIRCLE)) {
			return shapeService.getCircle();
		} else if (shapeName.equals(Globals.SQUARE)) {
			return shapeService.getSquare();
		} else {
			throw new IllegalArgumentException();
		}
	}

}
