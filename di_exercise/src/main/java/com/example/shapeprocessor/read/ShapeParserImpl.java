package com.example.shapeprocessor.read;

import javax.inject.Inject;

import com.example.shapeprocessor.Globals;
import com.example.shapeprocessor.shapes.Circle;
import com.example.shapeprocessor.shapes.OneParameterShape;
import com.example.shapeprocessor.shapes.Squere;

public class ShapeParserImpl implements ShapeParser {

	@Inject
	public ShapeParserImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public OneParameterShape parseShapeMessage(String message) {
		String[] msgParts = message.split(" ");
		int parameter = Integer.parseInt(msgParts[1]);
		if (msgParts[0].equals(Globals.CIRCLE)) {
			return new Circle(parameter);
		} else if (msgParts[0].equals(Globals.SQUARE)) {
			return new Squere(parameter);
		} else {
			throw new IllegalArgumentException("Parse error");
		}
	}

}
