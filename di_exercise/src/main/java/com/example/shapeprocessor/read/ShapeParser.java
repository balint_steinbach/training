package com.example.shapeprocessor.read;

import com.example.shapeprocessor.shapes.OneParameterShape;

public interface ShapeParser {
	public OneParameterShape parseShapeMessage(String message);
}
