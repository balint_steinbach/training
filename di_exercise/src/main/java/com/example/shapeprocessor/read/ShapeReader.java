package com.example.shapeprocessor.read;

import com.example.shapeprocessor.shapes.OneParameterShape;

public interface ShapeReader {

	OneParameterShape getShape(String string);

}
