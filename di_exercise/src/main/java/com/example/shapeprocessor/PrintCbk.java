package com.example.shapeprocessor;

import com.example.shapeprocessor.calc.StatCalculationCallback;
import com.example.shapeprocessor.calc.StatCalculationResult;
import com.example.shapeprocessor.shapes.OneParameterShape;

public class PrintCbk implements StatCalculationCallback {

	private OneParameterShape shape;

	public PrintCbk(OneParameterShape shape) {
		super();
		this.shape = shape;
	}

	@Override
	public void onCalculationFinished(StatCalculationResult statCalculationResult) {

		System.out.println(shape + " " + statCalculationResult);
	}

}