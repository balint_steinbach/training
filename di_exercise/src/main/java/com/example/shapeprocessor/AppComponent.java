package com.example.shapeprocessor;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = DaggerModule.class)
@Singleton
public interface AppComponent {
	App createApp();
}
