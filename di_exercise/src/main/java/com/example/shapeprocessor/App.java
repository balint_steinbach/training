package com.example.shapeprocessor;

import javax.inject.Inject;

import com.example.shapeprocessor.calc.StatCalculator;
import com.example.shapeprocessor.read.ShapeReader;
import com.example.shapeprocessor.shapes.OneParameterShape;

public class App {
	private ShapeReader shapeReader;
	private StatCalculator statCalculator;

	@Inject
	public App(ShapeReader shapeReader, StatCalculator statCalculator) {
		this.shapeReader = shapeReader;
		this.statCalculator = statCalculator;
	}

	public void userClickedOnCircle() {
		OneParameterShape shape = shapeReader.getShape(Globals.CIRCLE);
		statCalculator.submitCalculation(shape, new PrintCbk(shape));
	}

	public void userClickedOnSquare() {
		OneParameterShape shape = shapeReader.getShape(Globals.SQUARE);
		statCalculator.submitCalculation(shape, new PrintCbk(shape));
	}

}
