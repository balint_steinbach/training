package com.example.shapeprocessor.calc;

import com.example.shapeprocessor.shapes.OneParameterShape;

public interface StatCalculator {

	void submitCalculation(OneParameterShape shape, StatCalculationCallback statCalculationCallback);

}
