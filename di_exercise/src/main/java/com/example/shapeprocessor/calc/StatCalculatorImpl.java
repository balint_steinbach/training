package com.example.shapeprocessor.calc;

import java.util.concurrent.ExecutorService;

import javax.inject.Inject;

import com.example.shapeprocessor.shapes.OneParameterShape;

public class StatCalculatorImpl implements StatCalculator {

	private ExecutorService executorService;
	private CalculationTaskFactory calculationTaskFactory;

	@Inject
	public StatCalculatorImpl(ExecutorService executorService, CalculationTaskFactory calculationTaskFactory) {
		this.executorService = executorService;
		this.calculationTaskFactory = calculationTaskFactory;
	}

	@Override
	public void submitCalculation(OneParameterShape shape, StatCalculationCallback statCalculationCallback) {
		executorService.submit(calculationTaskFactory.createTask(shape, statCalculationCallback));
	}

}
