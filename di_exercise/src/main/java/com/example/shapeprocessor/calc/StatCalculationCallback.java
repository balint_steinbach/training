package com.example.shapeprocessor.calc;

public interface StatCalculationCallback {
	public void onCalculationFinished(StatCalculationResult statCalculationResult);
}
