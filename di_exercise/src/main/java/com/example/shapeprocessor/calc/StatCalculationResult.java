package com.example.shapeprocessor.calc;

public class StatCalculationResult {

	private double area;

	public void setArea(double area) {
		this.area = area;
	}

	public double getArea() {
		return area;
	}

	@Override
	public String toString() {
		return " area: " + area;
	}

}
