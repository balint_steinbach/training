package com.example.shapeprocessor.calc;

import javax.inject.Inject;

import com.example.shapeprocessor.service.StatCalculationService;
import com.example.shapeprocessor.shapes.OneParameterShape;

public class CalculationTask implements Runnable {

	private StatCalculationService calculationService;
	private OneParameterShape shape;
	private StatCalculationCallback statCalculationCallback;

	@Inject
	public CalculationTask(OneParameterShape shape, StatCalculationCallback statCalculationCallback,
			StatCalculationService calculationService) {
		this.shape = shape;
		this.statCalculationCallback = statCalculationCallback;
		this.calculationService = calculationService;
	}

	@Override
	public void run() {
		StatCalculationResult result = calculationService.calculate(shape);
		statCalculationCallback.onCalculationFinished(result);
	}

}
