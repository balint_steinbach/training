package com.example.shapeprocessor.calc;

import javax.inject.Inject;

import com.example.shapeprocessor.service.StatCalculationService;
import com.example.shapeprocessor.shapes.OneParameterShape;

public class CalculationTaskFactory {

	private StatCalculationService statCalculationService;

	@Inject
	public CalculationTaskFactory(StatCalculationService statCalculationService) {
		this.statCalculationService = statCalculationService;
	}

	public Runnable createTask(OneParameterShape shape, StatCalculationCallback statCalculationCallback) {
		return new CalculationTask(shape, statCalculationCallback, statCalculationService);
	}

}
