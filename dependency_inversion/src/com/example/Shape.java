package com.example;

public abstract class Shape {
	
	protected int parameter;
	
	public Shape(int parameter) {
		this.parameter = parameter;
	}

	public abstract double getArea();

	public abstract String getName();
	
	public int getParameter() {
		return parameter;
	}

}
