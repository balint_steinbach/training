package com.example;

public class Circle extends Shape {
	
	public Circle(int radius) {
		super(radius);
	}

	@Override
	public double getArea() {
		return parameter * parameter * Math.PI;
	}

	@Override
	public String getName() {
		return "Circle";
	}


}
