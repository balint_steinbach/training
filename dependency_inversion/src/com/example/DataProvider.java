package com.example;

public abstract class DataProvider {
	
	protected int parameter;
	
	public DataProvider(int parameter) {
		this.parameter = parameter;
	}

	public abstract Shape getShape();

}
