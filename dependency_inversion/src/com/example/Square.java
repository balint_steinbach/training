package com.example;

public class Square extends Shape{
	

	public Square(int side) {
		super(side);
	}

	
	@Override
	public double getArea(){
		return parameter * parameter;
	}


	@Override
	public String getName() {
		return "Square";
	}

}
