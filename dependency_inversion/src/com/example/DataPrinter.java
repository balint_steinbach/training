package com.example;

public interface DataPrinter {
	
	public void draw(Shape shape);

}
