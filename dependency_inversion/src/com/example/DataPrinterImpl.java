package com.example;

public class DataPrinterImpl implements DataPrinter{

	@Override
	public void draw(Shape shape) {
		System.out.println(shape.getName() + " parameter: " + shape.getParameter() + " Area: " + shape.getArea());
	}
}
