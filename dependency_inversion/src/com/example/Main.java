package com.example;

public class Main {
	
	private AreaCalculator areaCalculator;
	
	public Main() {
		int dataProviderParameter = 10;
		
		DataProvider dataProvider = new DataProviderImpl(dataProviderParameter);
		DataPrinter dataPrinter = new DataPrinterImpl();
		//injection in contructor
		areaCalculator = new AreaCalculator(dataProvider);
		//injection by setter
		areaCalculator.setDataPrinter(dataPrinter);
	}

	public static void main(String[] args) {
		new Main().run();
	}
	
	private void run(){
		areaCalculator.doTheJob();
	}
}
