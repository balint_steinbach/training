package com.example;

public class AreaCalculator {

	private DataProvider dataProvider;
	private DataPrinter dataPrinter;


	public AreaCalculator(DataProvider dataProvider) {
		this.dataProvider = dataProvider;
	}

	public void setDataPrinter(DataPrinter dataPrinter) {
		this.dataPrinter = dataPrinter;
	}

	public void doTheJob() {
		Shape shape = dataProvider.getShape();
		dataPrinter.draw(shape);
	}

}
