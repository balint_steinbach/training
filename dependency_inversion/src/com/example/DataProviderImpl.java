package com.example;

public class DataProviderImpl extends DataProvider{
	
	public DataProviderImpl(int parameter) {
		super(parameter);
	}

	@Override
	public Shape getShape() {
		if(parameter > 5){
			return new Square(parameter);
		} else {
			return new Circle(parameter);
		}
	}

}
