package com.example;

import org.junit.Assert;
import org.junit.Test;


public class SquareTest {
	
	@Test
	public void areaTest(){
		int side = 10;
		double area = side * side;
		
		Square square = new Square(side);
		
		Assert.assertEquals(area, square.getArea(), 0.00001);
	}

}
