package com.example;

import org.junit.Assert;
import org.junit.Test;



public class DataProviderTest {
	
	@Test
	public void testSquareDataProvider(){
		DataProvider dataProvider = new DataProviderImpl(10);
		
		Shape shape = dataProvider.getShape();
		
		Assert.assertEquals("Square", shape.getName());
		
	}
	
	@Test
	public void testCircleDataProvider(){
		DataProvider dataProvider = new DataProviderImpl(3);
		
		Shape shape = dataProvider.getShape();
		
		Assert.assertEquals("Circle", shape.getName());
		
	}
}
