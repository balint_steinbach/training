package com.example;

import org.junit.Test;

public class AreaCalculatorTest {
	
	@Test
	public void test(){
		DataProvider dataProvider = new DataProvider(10) {
			
			@Override
			public Shape getShape() {
				return new Circle(25);
			}
		};
		
		DataPrinter dataPrinter = new DataPrinterImpl();
		
		AreaCalculator areaCalculator = new AreaCalculator(dataProvider );
		
		areaCalculator.setDataPrinter(dataPrinter);
		
		areaCalculator.doTheJob();
		
		//Verify if job done
	}

}
