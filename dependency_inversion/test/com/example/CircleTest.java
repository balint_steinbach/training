package com.example;

import org.junit.Assert;
import org.junit.Test;


public class CircleTest {
	
	@Test
	public void areaTest(){
		int radius = 10;
		double area = radius * radius * Math.PI;
		
		Circle circle = new Circle(radius);
		
		Assert.assertEquals(area, circle.getArea(), 0.00001);
	}

}
