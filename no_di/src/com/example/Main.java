package com.example;

public class Main {
	
	private AreaCalculator areaCalculator;
	
	public Main() {
		areaCalculator = new AreaCalculator();
	}

	public static void main(String[] args) {
		new Main().run();
	}
	
	private void run(){
		areaCalculator.doTheJob();
	}
}
