package com.example;

public class AreaCalculator {

	private DataProvider dataProvider;
	private DataPrinter dataPrinter;

	public AreaCalculator() {
		int dataProviderParameter = 10;
		this.dataPrinter = new DataPrinter();
		this.dataProvider = new DataProvider(dataProviderParameter);
	}

	public void doTheJob() {
		Square square = new Square(dataProvider.getOneSide());
		dataPrinter.draw(square.getSide(), square.getSide() * square.getSide());
	}
}
