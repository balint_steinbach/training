package com.example.context;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class D {
	
	private E e;

	@Autowired
	public D(E e) {
		super();
		this.e = e;
	}
}
