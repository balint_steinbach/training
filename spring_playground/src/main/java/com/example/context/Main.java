
package com.example.context;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

	private AnnotationConfigApplicationContext ctx;

	public Main() {
		ctx = new AnnotationConfigApplicationContext(SpringConfig.class);
		for (String beanName : ctx.getBeanDefinitionNames()) {
			getDeps(beanName, 0);
		}
	}

	/**
	 * Main method.
	 */
	public static void main(String[] args) {
		new Main();
	}

	private void getDeps(String beanName, int depth) {
		String[] deps = ctx.getBeanFactory().getDependenciesForBean(beanName);
		BeanDefinition def = ctx.getBeanDefinition(beanName);
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < depth; i++) {
			sb.append("--");
		}
		String beanDisplayName = def.getBeanClassName();
		if (beanDisplayName == null) {
			beanDisplayName = beanName;
		}
		System.out.println(sb.toString() + beanDisplayName);
		for (String dependency : deps) {
			getDeps(dependency, depth + 1);
		}
	}

}
