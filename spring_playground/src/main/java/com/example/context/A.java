package com.example.context;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class A {

	private B b;

	private C c;

	private D d;

	private MyInterface myInterface;

	@Autowired
	public A(B b, C c, D d, MyInterface myInterface) {
		this.b = b;
		this.c = c;
		this.d = d;
		this.myInterface = myInterface;
		myInterface.foo();
	};
}
