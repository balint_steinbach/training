
package com.example.context;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.example.context")
class SpringConfig {

	@Qualifier("parameter")
	@Bean
	public String parameter() {
		return "some string";
	}

	@Bean
	public MyInterface myInterface() {
		return new MyInterfaceImpl();
	}
}
