package com.example.lifecycle_lazy;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
public class A implements InitializingBean, DisposableBean {

	public A() {
		System.out.println("A Constructor called");
	}

	public void afterPropertiesSet() throws Exception {
		System.out.println("A properties set");
	}

	public void destroy() throws Exception {
		System.out.println("A Destroyed");
	}
}
