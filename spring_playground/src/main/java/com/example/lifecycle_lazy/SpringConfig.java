package com.example.lifecycle_lazy;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.example.lifecycle_lazy")
class SpringConfig {

}
