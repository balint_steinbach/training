package com.example.graph.scan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class B {
	
	private C c;

	@Autowired
	public B(C C) {
		super();
		this.c = c;
	}
}
