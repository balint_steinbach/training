package com.example.graph.scan;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainAndLoop {

	/**
	 * Main method.
	 */
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		int TRESHOLD = 1000;
		System.out.println("Initializing Spring context.");
		
		for (int i = 0; i < TRESHOLD; i++) {

			ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfig.class);

			A a = ctx.getBean(A.class);
		}

		System.out.println("Spring context initialized in: " + (System.currentTimeMillis() - start) + " ms");

	}

}
