package com.example.graph.noscan;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class SpringConfig {

	@Bean
	public A getA(B b) {
		return new A(b);
	}

	@Bean
	public B getB(C c) {
		return new B(c);
	}

	@Bean
	public C getC(D d) {
		return new C(d);
	}

	@Bean
	public D getD() {
		return new D();
	}
}
