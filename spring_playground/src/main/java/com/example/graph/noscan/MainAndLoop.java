package com.example.graph.noscan;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainAndLoop {

	/**
	 * Main method.
	 */
	public static void main(String[] args) {
		int TRESHOLD = 1000;
		long start = System.currentTimeMillis();
		
		System.out.println("Initializing Spring context.");

		for (int i = 0; i < TRESHOLD; i++) {
			ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfig.class);

			A a = ctx.getBean(A.class);
		}

		System.out.println("Spring context initialized in: " + (System.currentTimeMillis() - start) + " ms");

	}

}
