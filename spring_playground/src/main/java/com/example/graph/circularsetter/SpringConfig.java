package com.example.graph.circularsetter;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.example.graph.circularsetter")
class SpringConfig {

}
