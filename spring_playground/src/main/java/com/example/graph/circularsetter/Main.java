package com.example.graph.circularsetter;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

	/**
	 * Main method.
	 */
	public static void main(String[] args) {
		long start = System.currentTimeMillis();
		System.out.println("Initializing Spring context.");

		ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfig.class);

		A a = ctx.getBean(A.class);

		System.out.println("Spring context initialized in: " + (System.currentTimeMillis() - start) + " ms");

	}

}
