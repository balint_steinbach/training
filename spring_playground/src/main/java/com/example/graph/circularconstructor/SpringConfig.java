package com.example.graph.circularconstructor;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.example.graph.circularconstructor")
class SpringConfig {

}
