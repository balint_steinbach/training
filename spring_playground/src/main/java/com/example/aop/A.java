package com.example.aop;

import org.springframework.stereotype.Component;

@Component
public class A {

	public A() {
	}

	public void foo() {
		System.out.println("foo called");
	}

	public void bar(String parameter) {
		System.out.println("bar called with param: " + parameter);
	}

}
