package com.example.aop;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

	/**
	 * Main method.
	 */
	public static void main(String[] args) {
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfig.class);

		A a = ctx.getBean(A.class);

		a.foo();

		a.bar("Parameter");
	}

}
