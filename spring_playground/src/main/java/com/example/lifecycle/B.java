package com.example.lifecycle;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
public class B implements InitializingBean, DisposableBean{

	public B() {
		System.out.println("B Constructor called");
	}

	public void afterPropertiesSet() throws Exception {
		System.out.println("B properties set");
	}

	public void destroy() throws Exception {
		System.out.println("B Destroyed");
	}
}
