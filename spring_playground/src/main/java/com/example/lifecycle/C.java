package com.example.lifecycle;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
public class C implements InitializingBean, DisposableBean{

	public C() {
		System.out.println("C Constructor called");
	}

	public void afterPropertiesSet() throws Exception {
		System.out.println("C properties set");
	}

	public void destroy() throws Exception {
		System.out.println("C Destroyed");
	}
}
