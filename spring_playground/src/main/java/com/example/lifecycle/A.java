package com.example.lifecycle;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class A implements InitializingBean, DisposableBean{
	
	@Autowired
	private C c;

	@Autowired
	public A(B b) {
		System.out.println("A Constructor called");
	}

	public void afterPropertiesSet() throws Exception {
		System.out.println("A properties set");
	}

	public void destroy() throws Exception {
		System.out.println("A Destroyed");
	}
}
