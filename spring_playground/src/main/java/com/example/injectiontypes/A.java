package com.example.injectiontypes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class A {
	
	private B b;
	
	@Autowired
	private C c;
	
	private D d;
	
	private String parameter;


	@Autowired
	public A(@Qualifier("parameter") String parameter, B b ) {
		this.b = b;
		this.parameter = parameter;
	};
	
	@Autowired
	public void setD(D d) {
		this.d = d;
	}

	public B getB() {
		return b;
	}

	public void setB(B b) {
		this.b = b;
	}

	public C getC() {
		return c;
	}

	public void setC(C c) {
		this.c = c;
	}

	public D getD() {
		return d;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}	
}
