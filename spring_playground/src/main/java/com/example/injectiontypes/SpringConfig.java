package com.example.injectiontypes;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.example.injectiontypes")
class SpringConfig {

	@Qualifier("parameter")
	@Bean
	public String getAParameter(){
		return "some string";
	}
}
