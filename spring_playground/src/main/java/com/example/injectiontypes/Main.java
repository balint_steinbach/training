package com.example.injectiontypes;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

	/**
	 * Main method.
	 */
	public static void main(String[] args) {

		ApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfig.class);

		A a = ctx.getBean(A.class);
		
		System.out.println("B: " + a.getB()+ " C: " + a.getC() + " D: " + a.getD() + " parameter: " + a.getParameter());

	}

}
