package com.example.factory;

import org.springframework.stereotype.Component;

@Component
public class B {
	private int cntr = 0;

	public void increment() {
		cntr++;
	}

	public void print() {
		System.out.println("B: " + cntr);
	}
}
