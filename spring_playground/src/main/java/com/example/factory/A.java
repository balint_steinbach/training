package com.example.factory;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class A {

	private ObjectFactory<B> bFactory;

	@Autowired
	public A(ObjectFactory<B> bFactory) {
		super();
		this.bFactory = bFactory;
	}

	public void increment() {
		bFactory.getObject().increment();
	}

	public void print() {
		bFactory.getObject().print();
	}

}
