package com.example.injectiontypes_methodinjection;
public abstract class PizzaShop {
   public abstract Pizza makePizza();
   public abstract Pizza makeVeggiePizza();
}