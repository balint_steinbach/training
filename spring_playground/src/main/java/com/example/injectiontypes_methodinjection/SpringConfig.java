package com.example.injectiontypes_methodinjection;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
class SpringConfig {
	
	@Bean
	@Scope("prototype")
	public Pizza pizza() {
	    return new Pizza();
	}

	@Bean
	public PizzaShop pizzaShop() {
	    // return new anonymous implementation of CommandManager with command() overridden
	    // to return a new prototype Command object
	    return new PizzaShop() {
			
			@Override
			public Pizza makeVeggiePizza() {
				Pizza veggiePizza = pizza();
				veggiePizza.setIsVeg(true);
				return veggiePizza;
			}
			
			@Override
			public Pizza makePizza() {
				Pizza pizza = pizza();
				pizza.setIsVeg(false);
				return pizza;
			}
		};
	}

	
}
