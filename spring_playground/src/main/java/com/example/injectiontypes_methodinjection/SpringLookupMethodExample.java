package com.example.injectiontypes_methodinjection;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringLookupMethodExample {
    public static void main(String[] args) {
    	ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);

        PizzaShop pizzaShop = (PizzaShop) context.getBean(PizzaShop.class);
        Pizza firstPizza = pizzaShop.makePizza();
        System.out.println("First Pizza: " + firstPizza);

        Pizza secondPizza = pizzaShop.makePizza();
        System.out.println("Second Pizza: " + secondPizza);
        
        Pizza veggiePizza = pizzaShop.makeVeggiePizza();
        System.out.println("Veggie Pizza: " + veggiePizza);
    }
}