package com.example.scope;

import org.springframework.stereotype.Component;

@Component
// singleton by default
public class SingletonClass {

	private int counter = 0;

	public void incrementCounter() {
		counter++;
	}

	public void print() {
		System.out.println("Singleton counter: " + counter);
	}

}
