package com.example.scope;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class PrototypeClass {
	private int counter = 0;

	public void incrementCounter() {
		counter++;
	}

	public void print() {
		System.out.println("Prototype counter: " + counter);
	}
}
