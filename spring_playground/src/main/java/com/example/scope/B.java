package com.example.scope;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class B {

	private PrototypeClass prototypeClass;
	private SingletonClass singletonClass;

	@Autowired
	public B(SingletonClass singletonClasnClass, PrototypeClass prototypeClass) {
		this.singletonClass = singletonClasnClass;
		this.prototypeClass = prototypeClass;
	}

	public void increment() {
		prototypeClass.incrementCounter();
		singletonClass.incrementCounter();
	}

	public void print() {
		prototypeClass.print();
		singletonClass.print();
	}
}
