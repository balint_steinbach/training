package com.example.scope;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

	/**
	 * Main method.
	 */
	public static void main(String[] args) {

		ConfigurableApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfig.class);

		A a = ctx.getBean(A.class);

		a.increment();
		a.print();

		B b = ctx.getBean(B.class);

		b.increment();
		b.print();
	}

}
