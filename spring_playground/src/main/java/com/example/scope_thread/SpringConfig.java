package com.example.scope_thread;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@ComponentScan("com.example.scope_thread")
@EnableAsync
class SpringConfig {
	
	@Bean
	public TaskExecutor taskExecutor() {
		ThreadPoolTaskExecutor pool = new ThreadPoolTaskExecutor();
		pool.setCorePoolSize(2);
		pool.setMaxPoolSize(2);
		pool.setThreadNamePrefix("Thread scopeed execution");
		pool.setWaitForTasksToCompleteOnShutdown(true);
		return pool;
	}
	
}
