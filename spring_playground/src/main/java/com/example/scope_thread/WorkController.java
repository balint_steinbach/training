package com.example.scope_thread;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class WorkController {
	
	private ObjectFactory<ThreadScopedWorker> workerFactory;

	@Autowired
	public WorkController(ObjectFactory<ThreadScopedWorker> workerFactory) {
		super();
		this.workerFactory = workerFactory;
	}
	
	@Async
	public void doWork(){
		workerFactory.getObject().work();
	}
}
