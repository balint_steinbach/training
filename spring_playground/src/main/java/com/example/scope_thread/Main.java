package com.example.scope_thread;

import org.springframework.beans.factory.config.Scope;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import org.springframework.context.support.SimpleThreadScope;

public class Main {

	/**
	 * Main method.
	 */
	public static void main(String[] args) {

		ConfigurableApplicationContext ctx = new AnnotationConfigApplicationContext(SpringConfig.class);
		
		Scope threadScope = new SimpleThreadScope();
		ctx.getBeanFactory().registerScope("thread", threadScope);

		WorkController a = ctx.getBean(WorkController.class);
		
		for(int i = 0; i< 10; i++) {
			a.doWork();
		}
		


	}

}
