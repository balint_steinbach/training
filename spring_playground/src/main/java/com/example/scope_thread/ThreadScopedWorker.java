package com.example.scope_thread;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("thread")
public class ThreadScopedWorker {
	
	private AtomicLong cntr = new AtomicLong();
	

	public void work() {
		System.out.println(Thread.currentThread().getName() + " cntr: " + cntr.incrementAndGet());
		
	}
}
