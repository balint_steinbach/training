
package com.example.context;

class ElectricHeater implements Heater {

	boolean heating;

	public void on() {
		System.out.println("~ ~ ~ Heating  ~ ~ ~");
		this.heating = true;
	}

	public void off() {
		this.heating = false;
	}

	public boolean isHot() {
		return heating;
	}
}