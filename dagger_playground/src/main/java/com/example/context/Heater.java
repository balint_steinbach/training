package com.example.context;

interface Heater {
	void on();

	void off();

	boolean isHot();
}
