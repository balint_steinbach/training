package com.example.context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module()
class DripCoffeeModule {

	@Provides
	@Singleton
	static Heater provideHeater() {
		return new ElectricHeater();
	}

	@Provides
	static Pump providePump(Thermosiphon pump) {
		return pump;
	}
}