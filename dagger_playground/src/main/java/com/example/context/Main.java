package com.example.context;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;

public class Main {

	@Component(modules = DripCoffeeModule.class)
	@Singleton
	interface CoffeeShop {
		CoffeeMaker maker();

		@Component.Builder
		interface Builder {
			@BindsInstance
			Builder temperature(@Named("temperature") Integer temperature);

			CoffeeShop build();
		}
	}

	public static void main(String[] args) {

		long start = System.currentTimeMillis();
		System.out.println("Initializing Dagger context.");

		DaggerMain_CoffeeShop.builder().temperature(80).build().maker().brew();

		System.out.println("Guice context initialized in: " + (System.currentTimeMillis() - start) + " ms");
	}

}
