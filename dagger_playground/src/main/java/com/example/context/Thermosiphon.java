package com.example.context;

import javax.inject.Inject;
import javax.inject.Named;

class Thermosiphon implements Pump {

	private final Heater heater;
	private Integer temperature;

	@Inject
	Thermosiphon(Heater heater, @Named("temperature") Integer temperature) {
		this.heater = heater;
		this.temperature = temperature;
	}

	public void pump() {
		if (heater.isHot()) {
			System.out.println("Temp is " + temperature);
			System.out.println("=> => pumping => =>");
		}
	}

}