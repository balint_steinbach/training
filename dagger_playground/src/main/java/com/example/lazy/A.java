package com.example.lazy;

import javax.inject.Inject;

import dagger.Lazy;

class A {

	private Lazy<B> b;

	@Inject
	public A(Lazy<B> b) {
		this.b = b;
		System.out.println("A is created");
	}

	public void foo() {
		b.get();
	}
}
