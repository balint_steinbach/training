package com.example.lazy;

import javax.inject.Inject;

public class B {

	@Inject
	public B() {
		System.out.println("B instance created");
	}

}
