package com.example.lazy;

public class Main {

	/**
	 * Main method.
	 */
	public static void main(String[] args) {

		// appcomponent should be created only once
		AppComponent appComponent = DaggerAppComponent.create();

		A a = appComponent.getA();

		System.out.println("Before calling a.foo");

		a.foo();
	}

}
