package com.example.circulardependencies;

import dagger.Component;

@Component
interface AppComponent {
	A getA();
}