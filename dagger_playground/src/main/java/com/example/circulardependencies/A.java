package com.example.circulardependencies;

import javax.inject.Inject;

public class A {

	@Inject
	public B b;

	@Inject
	public A() {
	}

	public void foo() {
		System.out.println("Foo called");
	}

}
