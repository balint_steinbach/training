package com.example.scope;

import javax.inject.Singleton;

import dagger.Component;

@Component
@Singleton
interface AppComponent {
	A getA();

	B getB();
}