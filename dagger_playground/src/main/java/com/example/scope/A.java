package com.example.scope;

import javax.inject.Inject;

class A {

	private PrototypeClass prototypeClass;
	private SingletonClass singletonClass;

	@Inject
	public A(SingletonClass singletonClass, PrototypeClass prototypeClass) {
		this.singletonClass = singletonClass;
		this.prototypeClass = prototypeClass;
	}

	public void increment() {
		prototypeClass.incrementCounter();
		singletonClass.incrementCounter();
	}

	public void print() {
		prototypeClass.print();
		singletonClass.print();
	}

}
