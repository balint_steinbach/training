package com.example.scope;

import javax.inject.Inject;

public class B {

	private PrototypeClass prototypeClass;
	private SingletonClass singletonClass;

	@Inject
	public B(SingletonClass singletonClasnClass, PrototypeClass prototypeClass) {
		this.singletonClass = singletonClasnClass;
		this.prototypeClass = prototypeClass;
	}

	public void increment() {
		prototypeClass.incrementCounter();
		singletonClass.incrementCounter();
	}

	public void print() {
		prototypeClass.print();
		singletonClass.print();
	}
}
