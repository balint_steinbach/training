package com.example.scope;

public class Main {

	/**
	 * Main method.
	 */
	public static void main(String[] args) {

		// appcomponent should be created only once
		AppComponent appComponent = DaggerAppComponent.create();

		A a = appComponent.getA();

		a.increment();
		a.print();

		B b = appComponent.getB();

		b.increment();
		b.print();
	}

}
