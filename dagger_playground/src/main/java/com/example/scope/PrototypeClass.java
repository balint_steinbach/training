package com.example.scope;

import javax.inject.Inject;

public class PrototypeClass {

	private int counter = 0;

	@Inject
	public PrototypeClass() {
		System.out.println("Prototype class created");
	}

	public void incrementCounter() {
		counter++;
	}

	public void print() {
		System.out.println("Prototype counter: " + counter);
	}
}
