package com.example;

public class DataProvider {
	
	private int parameter;
	
	public DataProvider(int parameter) {
		this.parameter = parameter;
	}

	public int getOneSide(){
		return parameter;
	}

}
