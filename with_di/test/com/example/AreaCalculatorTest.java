package com.example;

import org.junit.Test;

public class AreaCalculatorTest {
	
	@Test
	public void test(){
		//Mocking
		DataProvider dataProvider = new DataProvider(10);
		DataPrinter dataPrinter = new DataPrinter();
		
		AreaCalculator areaCalculator = new AreaCalculator(dataProvider );
		
		areaCalculator.setDataPrinter(dataPrinter);
		
		areaCalculator.doTheJob();
		
		//Verify if job done
	}

}
